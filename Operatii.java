
public class Operatii {
	
	public Polinom scadere(Polinom A,Polinom B) {
		int i=0;
		int j=0;
		Monom aux;
		Polinom AminusB=new Polinom();
		int nrA=A.nrMonoame();
		int nrB=B.nrMonoame();
		
		try {
			while(i<nrA && j<nrB) {
				double coeficientA=A.getMonom(i).getCoeficient();
				double putereA=A.getMonom(i).getPutere();
				
				double coeficientB=B.getMonom(j).getCoeficient();
				double putereB=B.getMonom(j).getPutere();	
				
				if(putereA == putereB) {
					aux=new Monom(coeficientA-coeficientB,putereA);
					i++;
					j++;
				}
				else
					if(putereA>putereB) {
						aux=new Monom(coeficientA,putereA);
						i++;
					}
					else {
						aux=new Monom(-coeficientB,putereB);
						j++;
					}
				AminusB.adunareMonoame(aux);
			}
			while(j<nrB)
			{
				double coeficientB=B.getMonom(j).getCoeficient();
				double putereB=B.getMonom(j).getPutere();
				aux=new Monom(-coeficientB,putereB);
				j++;
				AminusB.adunareMonoame(aux);
				
			}
			while(i<nrA) {
				double coeficientA=A.getMonom(i).getCoeficient();
				double putereA=A.getMonom(i).getPutere();
				aux=new Monom(coeficientA,putereA);
				i++;
				AminusB.adunareMonoame(aux);
			}
		}
		
		catch(Exception E) {}
			return AminusB;
				
					
		}
	
	public Polinom adunare(Polinom A,Polinom B) {
		int i=0;
		int j=0;
		Monom aux;
		Polinom AplusB=new Polinom();
		int nrA=A.nrMonoame();
		int nrB=B.nrMonoame();
		
		try {
			while(i<nrA && j<nrB) {
				double coeficientA=A.getMonom(i).getCoeficient();
				double putereA=A.getMonom(i).getPutere();
				
				double coeficientB=B.getMonom(j).getCoeficient();
				double putereB=B.getMonom(j).getPutere();	
				
				if(putereA == putereB) {
					aux=new Monom(coeficientA+coeficientB,putereA);
					i++;
					j++;
				}
				else
					if(putereA>putereB) {
						aux=new Monom(coeficientA,putereA);
						i++;
					}
					else {
						aux=new Monom(coeficientB,putereB);
						j++;
					}
				AplusB.adunareMonoame(aux);
			}
			while(j<nrB)
			{
				double coeficientB=B.getMonom(j).getCoeficient();
				double putereB=B.getMonom(j).getPutere();
				aux=new Monom(coeficientB,putereB);
				j++;
				AplusB.adunareMonoame(aux);
				
			}
			while(i<nrA) {
				double coeficientA=A.getMonom(i).getCoeficient();
				double putereA=A.getMonom(i).getPutere();
				aux=new Monom(coeficientA,putereA);
				i++;
				AplusB.adunareMonoame(aux);
			}
		}
		
		catch(Exception E) {}
			return AplusB;
	}
	
	public Polinom derivata(Polinom A) {
		int i=0;
		Monom aux;
		Polinom derivata=new Polinom();
		try {
			int nrA=A.nrMonoame();
			while(i<nrA) {
				double coeficientA=A.getMonom(i).getCoeficient();
				double putereA=A.getMonom(i).getPutere();
				i++;
				if(putereA!=0) {
					aux=new Monom(coeficientA*putereA,putereA-1);
					derivata.adunareMonoame(aux);
				}
				
			}
		}
		catch(Exception E) {}
		return derivata;
		
		
	}
	public Polinom inmultire(Polinom A,Polinom B){
		int i=0,j=0;
		Monom aux;
		Polinom AoriB=new Polinom();
		try {
			int nrA=A.nrMonoame();
			int nrB=B.nrMonoame();
			while(i<nrA) {
				double putereA=A.getMonom(i).getPutere();
				double coeficientA=A.getMonom(i).getCoeficient();
				i++;
				j=0;
				while(j<nrB) {
					double coeficientB=B.getMonom(j).getCoeficient();
					double putereB=B.getMonom(j).getPutere();
					aux=new Monom(coeficientA*coeficientB,putereA+putereB);
					AoriB.adunareMonoame(aux);
					j++;
				}
				
			}
			
		}
		catch(Exception E) {}
			return AoriB;
		
	}
	public Polinom integrala(Polinom A) {
		int i=0;
		Monom aux;
		Polinom integrata=new Polinom();
		try {
			int nrA=A.nrMonoame();
			while(i<nrA) {
				double coeficientA=A.getMonom(i).getCoeficient();
				double putereA=A.getMonom(i).getPutere();
				i++;
				if(putereA!=0) {
					aux=new Monom(coeficientA/(putereA+1),putereA+1);
					integrata.adunareMonoame(aux);
				}
				
			}
		}
		catch(Exception E) {}
		return integrata;
		
	}
	public String impartire(Polinom A,Polinom B)
	{
		int i=0;
		Operatii o=new Operatii();
		String impartit="";
		String rest="";
		Polinom AsupraB=new Polinom();
		try {
			while(A.getMonom(0).getPutere()>=B.getMonom(0).getPutere())
			{
				double coeficientB=B.getMonom(0).getCoeficient();
				double putereB=B.getMonom(0).getPutere();
				Monom temp1=new Monom(coeficientB,putereB);
				Monom temp2=A.getMonom(i);
				
				double coeficientAux=temp2.impartire(temp1).getCoeficient();
				double putereAux=A.getMonom(i).impartire(temp1).getPutere();
				
				Monom Aux=new Monom(coeficientAux,putereAux);
				Polinom aux2=new Polinom();
				aux2.adunareMonoame(Aux);
				
				AsupraB.adunareMonoame(Aux);
				Polinom aux3=new Polinom();
				aux3=o.inmultire(B, aux2);
				A=o.scadere(A, aux3);
				A.eliminareMonom(0);
				
			}
		}
		catch(Exception E) {}
		if(A.isEmpty())
			rest="0";
		else
			rest=A.toStringDouble();
		impartit=AsupraB.toStringDouble()+ " rest ="+rest;
	
		return impartit;
		}
	}
	
			