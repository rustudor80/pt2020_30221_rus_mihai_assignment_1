import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class MainClass implements ActionListener {
	private static Polinom PolinomA = new Polinom();
	private static Polinom PolinomB = new Polinom();
	private boolean citesteA = true, citesteB = true;
	private String work = "";
	
	JButton reset;
	
	JTextField editPolinomA, editPolinomB, resultTextField;
	
	JLabel labelA, labelB, result;
	
	JButton add, sub, mul, div;
	
	JButton help;
	
	
	GridBagConstraints c = new GridBagConstraints();

	public void addComponentsToPane(Container pane) {
		pane.setLayout(new GridBagLayout());

		
		labelA = new JLabel("Polinom A :", JLabel.LEFT);
		
		c.gridx = 0;
		c.gridy = 1;
		
		pane.add(labelA, c);

	
		editPolinomA = new JTextField(11);
		
		c.gridx++;
		
		pane.add(editPolinomA, c);
		
		

		labelB = new JLabel("Polinom B :", JLabel.LEFT);
		
		c.gridx = 0;
		c.gridy = 2;
		
		pane.add(labelB, c);

		
		editPolinomB = new JTextField(11);
		
		c.gridx++;
		
		pane.add(editPolinomB, c);

	
		add = new JButton("Adunare");
		
		c.gridx = 3;
		c.gridy = 1;
		
		pane.add(add, c);
		
		add.addActionListener(this);

	
		sub = new JButton("Scadere");
		
		c.gridx++;
		
		sub.addActionListener(this);
		
		pane.add(sub, c);

	
		mul = new JButton("Inmultire");
		
		c.gridx = 3;
		c.gridy = 2;
		
		mul.addActionListener(this);
		
		pane.add(mul, c);
		
		
		div = new JButton("Impartire");
		
		c.gridx++;
		
		div.addActionListener(this);
		pane.add(div, c);

		
		result = new JLabel("Rezultat :", JLabel.LEFT);
		
		c.gridx = 0;
		c.gridy = 3;
		
		pane.add(result, c);

		
		resultTextField = new JTextField(21);
		
		c.gridx++;
		
		c.ipadx = 15;
		c.ipady = 10;
		
		c.anchor = GridBagConstraints.LAST_LINE_START;
		resultTextField.setEditable(false);
		
		pane.add(resultTextField, c);
	
		
		c.gridx = 0;
		c.gridy = 0;
		
		pane.add(Box.createRigidArea(new Dimension(51, 0)));
		
	
		reset = new JButton("Reset");
		reset.addActionListener(this);
		
		c.gridy = 3;
		c.gridx = 6;
		
		c.ipadx = 1;
		c.ipady = 1;
		
		pane.add(reset, c);
	}

	public void actionPerformed(ActionEvent ev) {
		String textP, textQ;
		
		int auxMonom = 0, iMonom = 0;
		boolean areCoeficient = false; 
		boolean negative = false;//
		String inter = ""; 
		String interQ = "";
	
		Operatii o = new Operatii();

		final String mesajFormat = "Invalid format.";
		final String mesajGrad = "A Polinom's degree is smaller than B Polinom's degree";
		
		
		if (citesteA) {
			textP = editPolinomA.getText();
			if (textP.startsWith("-")) {
				areCoeficient = true;
				negative = true;
			}
			if (textP.startsWith("x")) {
				areCoeficient = true;
				negative = false;
			}
			if (areCoeficient == true) {

				if (negative == true) {
					auxMonom = -1;
					textP = textP.substring(1);
				}
				if (negative == false)
					auxMonom = 1;
			}
			
			StringTokenizer st1 = new StringTokenizer(textP, "x^");

			while (st1.hasMoreElements()) {

				inter = (String) st1.nextElement();

				StringTokenizer st2 = new StringTokenizer(inter, "+");
				StringTokenizer st3 = new StringTokenizer(inter, "-");

				if (inter.indexOf("-") != -1)
					while (st3.hasMoreElements()) {
						try {
							if (auxMonom == 0) {
								String copy = (String) st3.nextElement();
								auxMonom = Integer.parseInt(copy);
								auxMonom = -auxMonom;
								continue;
							}

							else if (iMonom == 0) {
								String copy = (String) st3.nextElement();
								iMonom = Integer.parseInt(copy);
								continue;
							}
						}
						
						catch (Exception e) {
							popUp(mesajFormat);
						}
						
						Monom monom = new Monom(auxMonom, iMonom);
						PolinomA.adunareMonoame(monom);
						iMonom = 0;
						auxMonom = 0;
					}
				else
					while (st2.hasMoreElements()) {

						if (auxMonom == 0) {
							String value = (String) st2.nextElement();
							try {
								auxMonom = Integer.parseInt(value);
							} catch (Exception e) {
								popUp(mesajFormat);
							}
							continue;
						}

						else if (iMonom == 0) {
							String value = (String) st2.nextElement();
							try {
								iMonom = Integer.parseInt(value);
							} catch (Exception e) {
								popUp(mesajFormat);
							}

							continue;
						}
						
						Monom monom = new Monom(auxMonom, iMonom);
						PolinomA.adunareMonoame(monom);
						
						iMonom = 0;
						auxMonom = 0;
					}

			}
			if (auxMonom != 0) {

				Monom monom = new Monom(auxMonom, iMonom);
				PolinomA.adunareMonoame(monom);

			}
			citesteA = false;
		}

		if (citesteB) {
			textQ = editPolinomB.getText();
			
			iMonom = 0;
			auxMonom = 0;
			areCoeficient = false;
			negative = false;

			if (textQ.startsWith("-")) {
				areCoeficient = true;
				negative = true;
			}
			if (textQ.startsWith("x")) {
				areCoeficient = true;
				negative = false;
			}

			if (areCoeficient == true) {

				if (negative == true) {
					auxMonom = -1;
					textQ = textQ.substring(1);
				}
				if (negative == false)
					auxMonom = 1;

			}

			StringTokenizer stQ1 = new StringTokenizer(textQ, "x^");

			while (stQ1.hasMoreElements()) {
				
				StringTokenizer stQ2 = new StringTokenizer(interQ, "+");
				StringTokenizer stQ3 = new StringTokenizer(interQ, "-");
				interQ = (String) stQ1.nextElement();

				

				if (interQ.indexOf("-") != -1)
					while (stQ3.hasMoreElements()) {

						try {
							if (auxMonom == 0) {
								String copy = (String) stQ3.nextElement();
								auxMonom = Integer.parseInt(copy);
								auxMonom = -auxMonom;
								continue;
							}

							else if (iMonom == 0) {
								String copy = (String) stQ3.nextElement();
								iMonom = Integer.parseInt(copy);

								continue;
							}
						} catch (Exception e) {
							popUp(mesajFormat);
						}

						Monom monom = new Monom(auxMonom, iMonom);
						PolinomB.adunareMonoame(monom);
						
						iMonom = 0;
						auxMonom = 0;
					}
				else
					while (stQ2.hasMoreElements()) {

						if (auxMonom == 0) {
							String value = (String) stQ2.nextElement();
							try {
								auxMonom = Integer.parseInt(value);
							} catch (Exception e) {
								popUp(mesajFormat);
							}
							continue;
						}

						else if (iMonom == 0) {
							String value = (String) stQ2.nextElement();
							try {
								iMonom = Integer.parseInt(value);
							} catch (Exception e) {
								popUp(mesajFormat);
							}
							continue;
						}
						
						Monom monom = new Monom(auxMonom, iMonom);
						PolinomB.adunareMonoame(monom);
						
						iMonom = 0;
						auxMonom = 0;
					}
			}

			if (auxMonom != 0) {
				Monom monom = new Monom(auxMonom, iMonom);
				PolinomB.adunareMonoame(monom);
			}

			citesteB = false;
		}

		if (ev.getSource() == reset) {
			citesteA = true;
			citesteB = true;
			
			editPolinomB.setText("");
			editPolinomA.setText("");
			
			work = "";
			
			PolinomA.reset();
			PolinomB.reset();
		}

		if (ev.getSource() == sub) {
			work = o.scadere(PolinomA, PolinomB).toString();
		}

		if (ev.getSource() == add) {
			work = o.adunare(PolinomA, PolinomB).toString();
		}

		if (ev.getSource() == mul) {
			work = o.inmultire(PolinomA, PolinomB).toString();
		}
		
		if (ev.getSource() == div) {
			if (PolinomA.getMonom(0).getPutere() < PolinomB.getMonom(0).getPutere()) {
				popUp(mesajGrad);
				work = "0  " + "r = " + PolinomA.toString();
			}

			else
				work = o.impartire(PolinomA, PolinomB);
		}
		
		
		
		resultTextField.setText(work);
	}

	

	private void createAndShowGUI() {

		JFrame frame = new JFrame("Calculator polinomial");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addComponentsToPane(frame.getContentPane());

		frame.pack();
		frame.setVisible(true);
	}
	
	private void popUp(String text) {

		JOptionPane.showMessageDialog(null, text, "Eroare!", JOptionPane.ERROR_MESSAGE);
	}
	
	public static void main(String[] args) {

		MainClass obj = new MainClass();
		obj.createAndShowGUI();
	}
}