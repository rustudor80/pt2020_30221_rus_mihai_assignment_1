import java.util.ArrayList;
import java.util.List;

public class Polinom {
	
	private int nrMonoame = 0;
	private List<Monom> argumente = new ArrayList<Monom>();
	
	public void adunareMonoame(Monom m) {
		++this.nrMonoame;
		this.argumente.add(m);
	}
	
public String toStringDouble() {
	
	String reprezentarePolinom="";
	String auxPutere="";
	String auxCoeficient="";
	for(Monom m:argumente) {
		if(m.getCoeficient()==0)
			continue;
		if((m.getCoeficient()!=1)&&(m.getCoeficient()!=-1))
			auxCoeficient=String.format("%.2g%n", m.getCoeficient()+"");
		else
			if(m.getCoeficient()==1)
				auxCoeficient="";
			else
				auxCoeficient="-";
		if((m.getPutere()!=0)&&(m.getPutere()!=1))
			auxPutere="x^"+(int)m.getPutere();
		else
			if(m.getPutere()==1)
				auxPutere="x";
			else
				auxPutere="";
		reprezentarePolinom+=auxCoeficient+auxPutere+"+";
	}
	if(argumente.isEmpty())
		reprezentarePolinom="0";
	if(reprezentarePolinom.compareTo("")==0)
		reprezentarePolinom="0";
	else
		if(reprezentarePolinom.length()>0)
			reprezentarePolinom=reprezentarePolinom.substring(0,reprezentarePolinom.length()-1);
	reprezentarePolinom=reprezentarePolinom.replace("+-", "-");
	return reprezentarePolinom;
}

	
public String toString(){
		
		boolean eMonomPozitiv = false;
		String reprezentarePolinom = "";
		String auxPutere = "";
		String auxCoeficient = "";
		
		for (Monom m : argumente){
			
			
			
			int coeficient = (int)m.getCoeficient();
			if(m.getCoeficient() == 0) 
				continue;
			if((coeficient != 1) && (coeficient != -1))
				auxCoeficient = coeficient + "";
			else
				if(coeficient == 1) {
					auxCoeficient = "";
					eMonomPozitiv = true;
					}
				else 
					auxCoeficient = "-"; 
			
				   	 
			if((m.getPutere() != 0) && (m.getPutere() != 1)) {
				
				auxPutere = "x^" + (int)m.getPutere();
			}
			else {
				if(m.getPutere() == 1) 
					auxPutere = "x";
				else 
					auxPutere = ""; 
			}
			
			if(coeficient == 1 && m.getPutere() == 0) 
				auxCoeficient = "1";
			if(coeficient == -1 && m.getPutere() == 0) 
				auxCoeficient = "-1";
			
			reprezentarePolinom = reprezentarePolinom+ auxCoeficient + auxPutere + "+";
			
			reprezentarePolinom = reprezentarePolinom.replace("+-", "-");
		}
		if(reprezentarePolinom.compareTo("") == 0) {
				
			if(eMonomPozitiv) 
				reprezentarePolinom = "1";
			else 
				reprezentarePolinom = "0"; 		
				}
		else
			if(reprezentarePolinom.length() > 0)
				reprezentarePolinom = reprezentarePolinom.substring(0,reprezentarePolinom.length()-1);
		
		reprezentarePolinom = reprezentarePolinom.replace("+-", "-");
		
		if(reprezentarePolinom.compareTo("-") == 0) 
			reprezentarePolinom = "-1";
		
		if(argumente.isEmpty()) {
		
			reprezentarePolinom = "0";
		}
		
		
		return reprezentarePolinom;
		
}


public Monom getMonom(int i){
	
	return this.argumente.get(i);
	
}
public int nrMonoame() {
	return this.nrMonoame;	
}


public void reset() {
	this.argumente.clear();
	this.nrMonoame = 0;
}

public void eliminareMonom(int i) {
	this.argumente.remove(i);
	
}


public boolean isEmpty() {
	
	return this.argumente.isEmpty(); 
	
}


}
