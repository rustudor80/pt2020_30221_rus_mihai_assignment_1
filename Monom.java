
public class Monom {
	
	private double coeficient;
	private double putere;
	
	public double getPutere(){
		return putere;
	}
	public void setPutere(double putere) {
		this.putere = putere;
	}
	public double getCoeficient() {
		return coeficient;
	}
	public void setCoeficient(double coeficient) {
		this.coeficient = coeficient;
	}
	Monom(double coeficient,double putere){
		setPutere(putere);
		setCoeficient(coeficient);
	}
	public Monom impartire(Monom temp) {
		Monom a = new Monom(this.coeficient / temp.getCoeficient(), putere - temp.getPutere());
		return a;
	}
	
}
